import pandas as pd
import math
from sklearn.metrics import log_loss
test1 = pd.read_csv("./test_rnn", sep=',')
test2 = pd.read_csv("./test10000", sep=',')

test1['y_pred'] = test1['y_pred'].apply(lambda x: x * 0.8)
test41 = pd.read_csv('../word_char_result/test_word_char1')
test42 = pd.read_csv('../word_char_result/test_word_char2')
test4 = pd.DataFrame(pd.concat([test41['y_pred'], test42['y_pred']], axis=1).mean(axis=1), columns=['y_pred'])
test4['y_pred'] = test4['y_pred'].apply(lambda x: x * 0.2)
test2['y_pred'] = test2['y_pred'].apply(lambda x: x * 0.5)



# test.columns = ["id", "question1", "question2", "is_duplicate"]
# test = test.iloc[:5000, :]
# preds = pd.read_csv('preds.csv', sep='\t', header=None, encoding='utf-8')

pred_list = test1['y_pred'] + test4['y_pred']
test_list = test1['label'].tolist()

print(log_loss(test_list, pred_list))
def readResult(y_test,results, threshold):
    index=0
    p=n=tp=tn=fp=fn=0.0

    for predLabel in results:
        if predLabel > threshold:
            predLabel = 1
        else :
            predLabel = 0
        if y_test[index]>0:
            p+=1
            if predLabel>0:
                tp+=1
            else:
                fn+=1
        else:
            n+=1
            if predLabel==0:
                tn+=1
            else:
                fp+=1
        index+=1

    acc=(tp+tn)/(p+n)
    precisionP=tp/(tp+fp)
    precisionN=tn/(tn+fn)
    recallP=tp/(tp+fn)
    recallN=tn/(tn+fp)
    gmean=math.sqrt(recallP*recallN)
    f_p=2*precisionP*recallP/(precisionP+recallP)
    f_n=2*precisionN*recallN/(precisionN+recallN)
    return ('{gmean:%s recallP:%s} {precP:%s fP:%s} acc:%s' %(gmean,recallP,precisionP,f_p,acc))
    # print('AUC %s' %average_precision_score(y_test,results))

    output=open('result.output','w')
    output.write('\n'.join(['%s' %r for r in results]))
for i in range(200, 600, 2):
    print(i/1000.0, readResult(test_list, pred_list, i/1000.0))


