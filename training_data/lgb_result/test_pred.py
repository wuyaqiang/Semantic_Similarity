from sklearn.metrics import log_loss, f1_score
import numpy as np
import lightgbm as lgb
import pandas as pd

df_train = pd.read_csv('../data/all_data2', encoding='utf-8', header=None, sep=' ')
df_train.columns = ['id', 'question1', 'question2', 'is_duplicate']
df_feature = pd.read_csv('../data/feature_table.csv', encoding='utf-8')

y_train = df_train['is_duplicate'].values[10000:]
y_test = df_train['is_duplicate'].values[:10000]
x_train = df_feature.values[10000:]
x_test = df_feature.values[:10000]


best_result = []
for i in range(10):
    gbm = lgb.Booster(model_file='../model/model' + str(i) + '.m')
    preds= gbm.predict(x_test, num_iteration=gbm.best_iteration)
    best_result.append(pd.DataFrame({"label": preds}))
pred_mean = pd.DataFrame(pd.concat(best_result, axis=1).mean(axis=1), columns=['label'])
pd.DataFrame({"label": y_test, "y_pred": pred_mean['label'].values}).to_csv('./test_lgb', encoding='utf-8', index=False)