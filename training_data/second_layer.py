# coding=utf8
import pandas as pd
from sklearn import linear_model
data_cnn = []
for i in range(10):
    data_temp = pd.read_csv('./cnn_result/vel' + str(i))
    data_cnn.append(data_temp)
data_cnn = pd.concat(data_cnn, axis=0)

data_rnn = []
for i in range(10):
    data_temp = pd.read_csv('./rnn_result/vel' + str(i))
    data_rnn.append(data_temp)
data_rnn = pd.concat(data_rnn, axis=0)
data_rnn.columns = ['label', 'y_pred_rnn']
train = pd.concat([data_cnn, data_rnn['y_pred_rnn']], axis=1)

data_lgb = []
for i in range(10):
    data_temp = pd.read_csv('./feature_extractor/lgb_result/val' + str(i) + '.csv')
    data_lgb.append(data_temp)
data_lgb = pd.concat(data_lgb, axis=0)
data_lgb.columns = ['label', 'y_pred_lgb']

data_word_char = []
for i in range(10):
    data_temp = pd.read_csv('./word_char_result/vel' + str(i))
    data_word_char.append(data_temp)
data_word_char = pd.concat(data_word_char, axis=0)
data_word_char.columns = ['label', 'y_pred_word_char']


train = pd.concat([data_cnn, data_rnn['y_pred_rnn'], data_lgb['y_pred_lgb'], data_word_char['y_pred_word_char']], axis=1)


test1 = pd.read_csv('cnn_result/test10000')
test2 = pd.read_csv('cnn_result/test_rnn')
test3 = pd.read_csv('./feature_extractor/lgb_result/test_lgb')

test41 = pd.read_csv('./word_char_result/test_word_char1')
test42 = pd.read_csv('./word_char_result/test_word_char2')
test4 = pd.DataFrame(pd.concat([test41['y_pred'], test42['y_pred']], axis=1).mean(axis=1), columns=['y_pred_word_char'])

test2.columns = ['label', 'y_pred_rnn']
test3.columns = ['label', 'y_pred_lgb']

test = pd.concat([test1, test2['y_pred_rnn'], test3['y_pred_lgb'], test4], axis=1)

# regr = linear_model.LinearRegression()
#
#
# regr.fit(train[['y_pred', 'y_pred_rnn', 'y_pred_lgb']], data_cnn['label']) # 注意此处.reshape(-1, 1)，因为X是一维的！
# prediction = regr.predict(test[['y_pred', 'y_pred_rnn', 'y_pred_lgb']])
# pd.DataFrame({'label':test['label'].values, 'y_pred': prediction}).to_csv('prediction3', index=False)

regr = linear_model.LogisticRegression()

regr.fit(train[['y_pred_rnn','y_pred_word_char']], data_cnn['label']) # 注意此处.reshape(-1, 1)，因为X是一维的！
prediction = regr.predict_proba(test[[ 'y_pred_rnn', 'y_pred_word_char']])
pd.DataFrame({'label':test['label'].values, 'y_pred': prediction[:, 1]}).to_csv('prediction4_log', index=False)